<?php
error_reporting(E_ALL);

include_once "engine/db/db_class.php";
include_once "engine/functions.php";
include_once "engine/global_config.php";

$db = new DB;
sec_session_start();

// Set time-out period (in seconds)
$inactive = SESSIONVALID;
 
// check to see if $_SESSION["timeout"] is set
if (isset($_SESSION["timeout"])) {
    // calculate the session's "time to live"
    $sessionTTL = time() - $_SESSION["timeout"];
    if ($sessionTTL > $inactive) {
        header("Location: logout.php");
    	exit();
    }
} 
 
$_SESSION["timeout"] = time();

if(!userLoginCheck($db, $_SESSION["userId"])){
	header("Location: login.php");
	exit();
}


?>

<!DOCTYPE html>
<html>
	<head>
		<title>WRIM - What really interests me</title>
		<meta charset="UTF-8" />
		<link href="css/style_main.css" type="text/css" rel="stylesheet" />
		<link href="css/style_menu.css" type="text/css" rel="stylesheet" />
		<link href="img/favicon.ico" type="image/png" rel="icon" />
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic' rel='stylesheet' type='text/css'>
		<script src="js/jquery-min.js" type="text/javascript"></script>
		<script src="js/functions.js" type="text/javascript" /></script>

	</head>
	<body>
		<?php 
			
			include("include/menu/menu.php");

			if(!empty($_GET["page"])){
				if($_GET["page"] == "impressum"){
					include("include/impressum.php");
				}else{
					include("include/main.php");
				}
			}else{
				include("include/main.php");
			}
		?>
	</body>
</html>