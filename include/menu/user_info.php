<div id="mainMenuUser">
	<div class="mainMenuUserProfileImage">
		<img src="<?php echo $db->requestUserData($_SESSION['userId'])['image']; ?>" id="circle" alt="Profile Image" />
	</div>
	<div class="mainMenuUserMotto">
		<p>I'm Nobody. Nobody's perfect. Therefore, I'm perfect.</p>
	</div>				
</div>