<div id="menu">
	<?php
	/*include "menu_main.php";
	/*include "menu_add.php";
	include "menu_search.php";
	include "menu_settings.php";
	*/
	?>
	<div id="mainMenu">
		<?php include "user_info.php"; ?>

		<div id="mainMenuContent"></div>
	</div>

	<div id="mainMenuBar">
		<div class="menuOption"><img id="openMenu" class="smallIcon" src="img/menu/menu.png" alt="Home Image" /></div>
		<div class="menuOption"><img id="openAdd" class="smallIcon" src="img/menu/add.png" alt="Add Image" /></div>
		<div class="menuOption"><img id="openSearch" class="smallIcon" src="img/menu/search.png" alt="Search Image" /></div>
		<div class="menuOption"><img id="openSettings" class="smallIcon" src="img/menu/settings.png" alt="Settings Image" /></div>
		<div class="menuOption"><img id="openHelp" class="smallIcon" src="img/menu/help.png" alt="Help Image" /></div>
		<a href="logout.php"><img class="smallIcon" src="img/menu/logout.png" alt="Logout Image" /></a>
	</div>
</div>
