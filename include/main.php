<?php

$userPosts = $db->requestUserPosts($_SESSION["userId"]);
$row1 = $row2 = $row3 = null;

echo "<div id='content'>";

if(!empty($userPosts)){

	$row = 1;

	foreach($userPosts as $post){
		// Get user data to display ( name, timestamp )
		$userPostData = $db->requestUserData($post[1]);

		// TODO: No request for each post, create list of optout posts / Different style for contentrows?
		if($db->requestUserOptoutPosts($post[1], $post[0]) == 0){
			if($row == 1){
				$row1 .= "<div class='contentBox flexi'>
				<div class='contentBoxHead'>
					<img src='".$userPostData["image"]."' alt='userImage' />
					<div class='contentBoxInfo'><p><span>".$userPostData["username"]."</span>".date(DATEFORMAT, strtotime($post[4]))."</p>
					<form action='engine/user/optout_post.php' method='POST'>
						<input type='image' src='img/icon/remove.png' name='postID' value='".$post[0]."' onsubmit='submit-form();' />
					</form>
					</div>
				</div>
				<div class='contentBoxContent'>
				<p>".$post[3]."</p></div></div>";
				$row++;
			}elseif($row == 2){
				$row2 .= "<div class='contentBox flexi'>
				<div class='contentBoxHead'><img src='".$userPostData["image"]."' alt='userImage' />
				<div class='contentBoxInfo'><p><span>".$userPostData["username"]."</span>".date(DATEFORMAT, strtotime($post[4]))."</p>
				<form action='engine/user/optout_post.php' method='POST'>
					<input type='image' src='img/icon/remove.png' name='postID' value='".$post[0]."' onsubmit='submit-form();' />
				</form>
				</div></div>
				<div class='contentBoxContent'>
				<p>".$post[3]."</p></div></div>";
				$row++;
			}else{
				$row3 .= "<div class='contentBox flexi'>
				<div class='contentBoxHead'><img src='".$userPostData["image"]."' alt='userImage' />
				<div class='contentBoxInfo'><p><span>".$userPostData["username"]."</span>".date(DATEFORMAT, strtotime($post[4]))."</p>
				<form action='engine/user/optout_post.php' method='POST'>
					<input type='image' src='img/icon/remove.png' name='postID' value='".$post[0]."' onsubmit='submit-form();' />
				</form>
				</div></div>
				<div class='contentBoxContent'>
				<p>".$post[3]."</p></div></div>";
				$row = 1;
			}
		}
	}

	echo "<div class='contentRow' class='flexbox'>".$row1."</div><div class='contentRow' class='flexbox'>".$row2."</div><div class='contentRow' class='flexbox'>".$row3."</div>";
}else{
	echo "<div class='contentRow'><div class='contentBox flexi'><div class='contentBoxContent'><p>Currently there are no new posts</p></div></div></div>";
}



echo "</div>";

?>
