$(document).ready(function(){

    $("#content").click(function(){
        $("#menu").removeClass("slideClass");
    });

    var openMenuClick = 0;
    var menuClick = false;

	$("#openMenu").click(function(){
        if(openMenuClick == 0 && menuClick == false){
            openMenuClick++;
            $("#menu").toggleClass("slideClass");
        }else if(openMenuClick > 1){
            $("#menu").toggleClass("slideClass");
            openMenuClick = 0;
        }else if(openMenuClick == 1 && menuClick == false){
            $("#menu").toggleClass("slideClass");
            openMenuClick = 0;
        }else{
            openMenuClick++;
        }

        $("#mainMenuContent").load("../include/menu/menu_main.php");
        menuClick = false;
    });

    $("#openAdd").click(function(){
        menuClick = true;
    	if(!$("#menu").hasClass("slideClass")){
            $("#menu").toggleClass("slideClass");
        }
        $("#mainMenuContent").load("../include/menu/menu_add.php");
    });

    $("#openSearch").click(function(){
        menuClick = true;
        if(!$("#menu").hasClass("slideClass")){
            $("#menu").toggleClass("slideClass");
        }
        $("#mainMenuContent").load("../include/menu/menu_search.php");
    });

    $("#openSettings").click(function(){
        menuClick = true;
        if(!$("#menu").hasClass("slideClass")){
            $("#menu").toggleClass("slideClass");
        }
        $("#mainMenuContent").load("../include/menu/menu_settings.php");
    });
});

function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (o.scrollHeight)+"px";
    console.log("Height: " + o.style.height + "- " + o.scrollHeight);
}
