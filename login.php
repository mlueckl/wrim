<?php
include_once "engine/db/db_class.php";
include_once "engine/functions.php";

$db = new DB;
sec_session_start();

// Check if User has an valid Session
if(isset($_SESSION["userId"])){
	if(!userLoginCheck($db, $_SESSION["userId"])){
		header("Location: ./");
	}
}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>WRIM - What really interests me</title>
		<meta charset="UTF-8" />
		<link href="css/style_login.css" type="text/css" rel="stylesheet" />
		<link href="img/favicon.ico" type="image/png" rel="icon" />
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<div id="content">
			<div id="contentHead">
				<h1>WRIM</h1>
				<p><b>W</b>hat <b>R</b>eally <b>I</b>ntersts <b>Me</b>
			</div>
			<div id="contentBody">
				<div class="box-half">
					<h2>Login</h2>
					<form method="POST" action="engine/user/login_process.php">
						<input type="text" name="login[1]" placeholder="Username" />
						<input type="password" name="password[1]" placeholder="Password" />
						<button class="submitButton" type="submit" name="submitLogin">Login</button>
						<!--
						<div class="bigInput">
							<input type="checkbox" name="rememberUser" value="rememberUser"><p>Remember me</p>
						</div>
						-->
					</form>
				</div>
				<div class="box-half">
					<h2>Register</h2>
					<form method="POST" action="engine/user/register_process.php">
						<input type="text" name="login[2]" placeholder="User Name" />
						<input type="text" name="email" placeholder="E-Mail Adresse" />
						<input type="password" name="password[2]" placeholder="Password" />
						<input type="password" name="password[2]" placeholder="Repeat Password" />
						<button class="submitButton" type="submit" name="submitRegister">Registrieren</button>
					</form>
				</div>

				<div class="box-full message fixHeader">
					<?php
						// Check if User has tried to register
						if(isset($_SESSION["CreationStatus"])){
							if($_SESSION["CreationStatus"] == "user_created"){
								echo "<p class='goodMessage'>You are now successfully registered. You can now login.</p>";
								unset($_SESSION["CreationStatus"]);
							}else if($_SESSION["CreationStatus"] == "error_on_user_creation"){
								echo "<p class='badMessage'>Sorry but there was a problem by the registration. Please try again.</p>";
								unset($_SESSION["CreationStatus"]);
							}else if($_SESSION["CreationStatus"] == "error_user_exists"){
								echo "<p class='badMessage'>Sorry but this Username is already used. Please try a different one.</p>";
								unset($_SESSION["CreationStatus"]);
							}else{
								echo "<p class='badMessage'>Please fill out all fields.</p>";
								unset($_SESSION["CreationStatus"]);
							}
						}

						// Check if User has tried to login
						if(isset($_SESSION["LoginStatus"])){
							if($_SESSION["LoginStatus"] == "wrong_login"){
								echo "<p class='badMessage'>Username or Password was wrong. Please try again.</p>";
								unset($_SESSION["LoginStatus"]);
							}else if($_SESSION["LoginStatus"] == "no_user_match"){
								echo "<p class='badMessage'>Sorry but the user does not exist</p>";
								unset($_SESSION["LoginStatus"]);
							}else{
								echo "<p class='badMessage'>Please input your Username and Password.</p>";
								unset($_SESSION["LoginStatus"]);
							}
						}
					?>
				</div>
			</div>
			<div id="contentFooter">
			</div>
		</div>
	</body>
</html>
