<?php

include_once "db/db_class.php";

function sec_session_start() {
    $session_name = 'wrim_superBrain';
    $secure = SECURE;
    $httponly = true;
    
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }

    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    
    session_name($session_name);
    session_start();
    session_regenerate_id();
}

function startUserSession($userId, $password) { 
    $user_browser = $_SERVER['HTTP_USER_AGENT'];
    $_SESSION["userId"] = $userId;
    $_SESSION["loginString"] = hash('sha512', $password.$user_browser);
    
    return true;
}

function userLoginCheck($db, $userId) {
    if (isset($_SESSION['loginString'], $_SESSION["userId"])){
        $loginString = $_SESSION['loginString'];
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
        $loginCheck = hash('sha512',$db->requestUserData($userId)["password"].$user_browser);

        if ($loginCheck == $loginString) {
            return true;
        } else {
            return false;
        }
    }
}

function userLogout(){
    header("Location: logout.php");
}

?>