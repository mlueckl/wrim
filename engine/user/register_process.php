<?php
include_once "../global_config.php";
include_once "../functions.php";

$db = new DB;
sec_session_start();

if(!empty($_POST)){
	if(!empty($_POST['login'][2]) && !empty($_POST['email']) && !empty($_POST['password'][2])){
		if($db->requestUserID($_POST['login'][2]) == null){
			$userCreated = $db->createUser($_POST['login'][2], $_POST['email'], password_hash($_POST['password'][2], PASSWORD_DEFAULT));
			if($userCreated){
				$_SESSION["CreationStatus"] = "user_created";
				header("Location: ".URL);
				exit();
			}else{
				$_SESSION["CreationStatus"] = "error_on_user_creation";
				header("Location: ".URL);
				exit();
			}
		}else{
		    $_SESSION["CreationStatus"] = "error_user_exists";
		    header("Location: ".URL);
		    exit();
		}
	}else{
		$_SESSION["CreationStatus"] = "missingdata_user_creation";
		header("Location: ".URL);
		exit();
	}
}
?>
