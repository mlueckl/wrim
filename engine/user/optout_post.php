<?php
include_once "../global_config.php";
include_once "../functions.php";

$db = new DB;
sec_session_start();

if(isset($_POST)){
	if(!empty($_POST["postID"])){
		if(isset($_SESSION["userId"])){
			$db->userOptoutPost($_SESSION["userId"], $_POST["postID"]);
			header("Location: ".URL);
		}
	}
}

?>