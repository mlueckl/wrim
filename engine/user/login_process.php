<?php
include_once "../global_config.php";
include_once "../functions.php";

$db = new DB;
sec_session_start();

if(!empty($_POST)){

	// Check if Login and Password are not empty
	if(!empty($_POST['login'][1]) && !empty($_POST['password'][1])){

		// Check if User exists in DB
		if($db->requestUserId($_POST['login'][1]) != null){
			$userId = $db->requestUserId($_POST['login'][1]);

			// Log user login
			$db->logLastUserLogin($userId);

			// Check if User name and Password match
			if(password_verify($_POST["password"][1], $db->requestUserData($userId)["password"])){

				// Start User Session and redirect to LoginPage
				if(startUserSession($db->requestUserId($_POST['login'][1]), $db->requestUserData($userId)["password"])){
					header("Location: ".URL);
					exit();
				}
			}else{
				$_SESSION["LoginStatus"] = "wrong_login";
				header("Location: ".URL);
			}
		}else{
			$_SESSION["LoginStatus"] = "no_user_match";
			header("Location: ".URL);
			exit();
		}
	}else{
		$_SESSION["LoginStatus"] = "no_data";
		header("Location: ".URL);
		exit();
	}
}
?>