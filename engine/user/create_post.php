<?php
header("Content-Type: text/html; charset=utf-8");

include_once "../global_config.php";
include_once "../functions.php";

$db = new DB;
sec_session_start();

print_r($_POST);

if(isset($_POST)){
	if(!empty($_POST["userPostMessage"])){
		if(isset($_SESSION["userId"])){
			$username = $db->requestUserData($_SESSION["userId"]);
			$db->createUserPost($_SESSION["userId"], $username["username"], addslashes($_POST["userPostMessage"]));
			$userPostCreated = true;
			header("Location: ".URL);
		}
	}
}

?>