<?php
require_once "db_config.php";

if( !class_exists("DBInsert")){
	class DB{
		public function __construct(){
			$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);

			if($mysqli->connect_errno){
				printf("Connection error %s\n", $mysqli->connect_error);
				exit();
			}

			// TODO: only check on startup not for every DB
			// Check if Table exists otherwise create it
			if($mysqli->query("SHOW TABLES LIKE 'user'")){
				$mysqli->query("CREATE TABLE wrim.user(userID INT NOT NULL AUTO_INCREMENT, username TEXT NOT NULL, password TEXT NOT NULL, email TEXT NOT NULL, name TEXT NOT NULL, lastname TEXT NOT NULL, image VARCHAR(100) NOT NULL, PRIMARY KEY(userID))");
			}

			// Check if Table exists otherwise create it
			if($mysqli->query("SHOW TABLES LIKE 'user_post'")){
				$mysqli->query("CREATE TABLE wrim.user_post(postID INT NOT NULL AUTO_INCREMENT, userID INT NOT NULL, username TEXT NOT NULL, message TEXT NOT NULL, timestamp TIMESTAMP NOT NULL, PRIMARY KEY(postID))");
			}

			// Check if Table exists otherwise create it
			if($mysqli->query("SHOW TABLES LIKE 'user_lastlogin'")){
				$mysqli->query("CREATE TABLE wrim.user_lastlogin(lastloginID INT NOT NULL AUTO_INCREMENT, userID INT NOT NULL, timestamp TIMESTAMP NOT NULL, PRIMARY KEY(lastloginID))");
			}

			$this->connection = $mysqli;
		}

		// CREATE TODO: error handling if insert fails
		public function createUser($username, $email, $password){
			$mysqli = $this->connection;
			$createUser = "INSERT IGNORE INTO user(username, email, password)VALUES('$username', '$email', '$password')";
			$result = $mysqli->query($createUser);

			return $result;
		}

		public function createUserPost($userID, $username, $message){
			$mysqli = $this->connection;
			$createUserPost = "INSERT INTO user_post(userID, username, message)VALUES('$userID', '$username', '$message')";
			$result = $mysqli->query($createUserPost);

			return $result;
		}

		public function logLastUserLogin($userID){
			$mysqli = $this->connection;
			$logLastUserLogin = "INSERT INTO user_lastlogin(userID)VALUES('$userID')";
			$result = $mysqli->query($logLastUserLogin);

			return $result;
		}

		public function userOptoutPost($userID, $postID){
			$mysqli = $this->connection;
			$logLastUserLogin = "INSERT INTO user_optout_post(userID, postID)VALUES('$userID', '$postID')";
			$result = $mysqli->query($logLastUserLogin);

			return $result;
		}

		// REQUEST
		public function requestUserId($username){
			$mysqli = $this->connection;
			$query = "SELECT userID FROM user WHERE username='".$username."'";
			$result = $mysqli->query($query);
			if($result->num_rows > 0){
				$obj = $result->fetch_object();
				return $obj->userID;
			}else{
				return null;
			}
		}

		public function requestUserData($userID){
			$mysqli = $this->connection;
			$query = "SELECT * FROM user WHERE userID='$userID'";
			$result = $mysqli->query($query);
			if($result->num_rows > 0){
				$assoc = $result->fetch_assoc();
				return $assoc;
			}else{
				return null;
			}
		}

		public function requestUserPosts($userID){
			$mysqli = $this->connection;
			$check_if_table_exists = $mysqli->query("SHOW TABLES LIKE 'user_post'");

			if($check_if_table_exists->num_rows == 1){
				$result = $mysqli->query("SELECT * FROM user_post WHERE userID='$userID'");
				$userPosts = $result->fetch_all();

				return $userPosts;
			}else{
				return null;
			}
		}

		public function requestUserOptoutPosts($userID, $postID){
			$mysqli = $this->connection;
			$check_if_table_exists = $mysqli->query("SHOW TABLES LIKE 'user_optout_post'");

			if($check_if_table_exists->num_rows == 1){
				$result = $mysqli->query("SELECT EXISTS(SELECT optoutID FROM user_optout_post WHERE postID='$postID' AND userID='$userID')");
				$userPosts = $result->fetch_all();

				return $userPosts[0][0];
			}else{
				return null;
			}
		}


		// CLOSE
		public function closeConnection(){
			$mysqli->close();
		}
	}
}
?>
